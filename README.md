# LoDa.OS - Powergeneration

Arduino-Code zur Installation auf dem ESP32, um die Daten zur Stromerzeugung (Simulation) über eine RGB-LED anzuzeigen.

Zum Erstellen der Quellen für den ESP32 wird die Open Source Entwicklungsumgebung von PlatformIO (https://platformio.org/) benötigt.