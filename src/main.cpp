/*
  Configurable LoDa OS to select data source and controll rgb led for visualisation 
  Copyright (c) 2020, Lumo Box
  https://gitlab.com/loda.run/loda.os-powergeneration

  This software is released under the ??? License.
  https://LINK_TO_THE_LICENSE
*/
#include <Arduino.h>
#include "LodaConnectLabels.h"

#if defined(ARDUINO_ARCH_ESP8266)
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#elif defined(ARDUINO_ARCH_ESP32)
#include <WiFi.h>
#include <WebServer.h>
#endif

#include <AutoConnect.h>
#include <HTTPClient.h>
#include <Preferences.h>
#include <Adafruit_NeoPixel.h>

#define LED_PIN 13
#define LED_BRIGHTNESS 50 // value between 0 and 255

Preferences pref;
Adafruit_NeoPixel pixel = Adafruit_NeoPixel(1, LED_PIN, NEO_RGB + NEO_KHZ800);

static const char AUX_PRESET[] PROGMEM = R"(
{
  "title": "Datenquelle",
  "uri": "/preset",
  "menu": true,
  "element": [
    {
      "name": "caption",
      "type": "ACText",
      "value": "W&auml;hle die Datenquelle f&uuml;r die LED aus.",
      "style": "font-family:Arial;font-weight:bold;text-align:center;margin-bottom:10px;color:DarkSlateBlue"
    },
    {
      "name": "preset",
      "type": "ACSelect",
      "label": "Datenquelle",
      "option": [],
      "selected": 1
    },
    {
      "name": "newline",
      "type": "ACElement",
      "value": "<br>"
    },
    {
      "name": "start",
      "type": "ACSubmit",
      "value": "OK",
      "uri": "/start"
    }
  ]
}
)";

typedef struct {
  const char* title;
  const char* server;
  const char* datakey;
  const int   timeout;
} Preset_t;


static const Preset_t PRES[] = {
  { "Stromerzeugung-Regenerativ-Gesamt", "https://oklabpdm.uber.space/powergeneration/rest/total_renewables.json", "total_renewables", 5 },
  { "Stromerzeugung-Konventionell-Gesamt", "https://oklabpdm.uber.space/powergeneration/rest/total_conventional.json", "total_conventional", 5 },
  { "Stromerzeugung-Gesamt", "https://oklabpdm.uber.space/powergeneration/rest/total_all.json", "total_all", 5 },

  { "Stromerzeugung-Regenerativ-Biomasse", "https://oklabpdm.uber.space/powergeneration/rest/biomass.json", "biomass", 5 },
  { "Stromerzeugung-Regenerativ-Wasserkraft", "https://oklabpdm.uber.space/powergeneration/rest/hydropower.json", "hydropower", 5 },
  { "Stromerzeugung-Regenerativ-Wind", "https://oklabpdm.uber.space/powergeneration/rest/wind.json", "wind", 5 },
  { "Stromerzeugung-Regenerativ-Wind-Offshore", "https://oklabpdm.uber.space/powergeneration/rest/wind_offshore.json", "wind_offshore", 5 },
  { "Stromerzeugung-Regenerativ-Wind-Onshore", "https://oklabpdm.uber.space/powergeneration/rest/wind_onshore.json", "wind_onshore", 5 },
  { "Stromerzeugung-Regenerativ-Photovoltaik", "https://oklabpdm.uber.space/powergeneration/rest/photovoltaics.json", "photovoltaics", 5 },
  { "Stromerzeugung-Regenerativ-Sonstige", "https://oklabpdm.uber.space/powergeneration/rest/other_renewables.json", "other_renewables", 5 },

  { "Stromerzeugung-Konventionell-Kernenergie", "https://oklabpdm.uber.space/powergeneration/rest/nuclear.json", "nuclear", 5 },
  { "Stromerzeugung-Konventionell-Kohle", "https://oklabpdm.uber.space/powergeneration/rest/coal.json", "coal", 5 },
  { "Stromerzeugung-Konventionell-Braunkohle", "https://oklabpdm.uber.space/powergeneration/rest/lignite.json", "lignite", 5 },
  { "Stromerzeugung-Konventionell-Steinkohle", "https://oklabpdm.uber.space/powergeneration/rest/hardcoal.json", "hardcoal", 5 },
  { "Stromerzeugung-Konventionell-Erdgas", "https://oklabpdm.uber.space/powergeneration/rest/natural_gas.json", "natural_gas", 5 },
  { "Stromerzeugung-Konventionell-Pumpspeicher", "https://oklabpdm.uber.space/powergeneration/rest/pumped_storage.json", "pumped_storage", 5 },
  { "Stromerzeugung-Konventionell-Sonstige", "https://oklabpdm.uber.space/powergeneration/rest/other_conventional.json", "other_conventional", 5 }
};

String presetTitle = "";
String presetServer = "";
String presetDatakey = "";
int presetTimeout = 5;
int loopCount = 0;

// Init current values
typedef struct {
  int r;
  int g;
  int b;
  // uint8_t l;
} RGB;

typedef struct {
  float min;
  float max;
  float avg;
} STATS;

String  currentDate   = String('N/A');
float   currentValue  = 0;
float   currentMin    = 0;
float   currentMax    = 0;
float   currentAvg    = 0;
RGB     currentColor  = {0,0,0};
char    currentColorString[100];


// board ID
char ssid[15]; //Create a Unique AP from MAC address

// Declare functions
void createSSID();
void doRequest( String url );
void colorFade(uint8_t r, uint8_t g, uint8_t b);

#if defined(ARDUINO_ARCH_ESP8266)
ESP8266WebServer Server;
#elif defined(ARDUINO_ARCH_ESP32)
WebServer Server;
#endif

AutoConnect       Portal(Server);
AutoConnectConfig Config;       // Enable autoReconnect supported on v0.9.4
AutoConnectAux    Preset;

void rootPage() {
  String  content =
    "<html>"
    "<head>"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
   "<script type=\"text/javascript\">"
    "setTimeout(\"location.reload()\", {{Timeout}});"
   "</script>"
      "<style>"
        "body { background-color: #222; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-weght: 300; }"
        ".content { width: 450px; margin: 25px auto; padding: 15px; background-color: #eee; border-radius: 5px; box-shadow: inset 3px 3px 25px -5px rgba(40,40,40,0.6);}"
        "table { margin: 25px auto; }"
        "th, td { text-align: right; }"
      "</style>"
    "</head>"
    "<body style=\"background-color: {{currentColor}}\"><div class=\"content\">"
    "<h2 align=\"center\" style=\"margin:20px;\"><svg width=\"200\" height=\"150\" viewBox=\"0 0 400 250\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;\">    <defs>          <linearGradient id=\"logo-gradient\" x1=\"50%\" y1=\"0%\" x2=\"50%\" y2=\"100%\" >                         <stop offset=\"0%\" stop-color=\"#7A5FFF\">                <animate attributeName=\"stop-color\" values=\"#7A5FFF; #01FF89; #7A5FFF\" dur=\"4s\" repeatCount=\"indefinite\"></animate>            </stop>            <stop offset=\"100%\" stop-color=\"#01FF89\">                <animate attributeName=\"stop-color\" values=\"#01FF89; #7A5FFF; #01FF89\" dur=\"4s\" repeatCount=\"indefinite\"></animate>            </stop>        </linearGradient>    </defs>        <path fill=\"url('#logo-gradient')\" d=\"M285.869,153.964c2.478,-3.775 5.328,-7.273 8.55,-10.495c5.538,-5.537 11.889,-9.975 19.055,-13.314c7.166,-3.339 14.984,-5.008 23.453,-5.008c8.631,0 16.53,1.669 23.696,5.008c7.166,3.339 13.518,7.777 19.055,13.314c5.537,5.537 9.975,11.889 13.314,19.055c3.339,7.166 5.008,15.065 5.008,23.696l0,60.829l-12.215,0l0,-60.829c0,-6.84 -1.384,-13.192 -4.153,-19.055c-2.768,-5.863 -6.27,-10.911 -10.504,-15.146c-4.235,-4.234 -9.283,-7.736 -15.147,-10.505c-5.863,-2.768 -12.214,-4.152 -19.054,-4.152c-6.678,0 -12.948,1.384 -18.811,4.152c-5.863,2.769 -10.912,6.271 -15.146,10.505c-4.886,4.235 -8.551,9.283 -10.993,15.146c-2.443,5.863 -3.665,12.215 -3.665,19.055c0,6.678 1.222,12.948 3.665,18.811c2.442,5.863 6.107,10.912 10.993,15.146c4.234,4.886 9.283,8.55 15.146,10.993c5.863,2.443 12.133,3.665 18.811,3.665c6.84,0 13.191,-1.222 19.054,-3.665c5.864,-2.443 10.912,-6.107 15.147,-10.993l8.55,8.55c-5.537,5.538 -11.889,9.976 -19.055,13.314c-7.166,3.339 -15.065,5.008 -23.696,5.008c-8.469,0 -16.287,-1.669 -23.453,-5.008c-7.166,-3.338 -13.517,-7.776 -19.055,-13.314c-5.537,-5.537 -9.975,-11.889 -13.314,-19.055l-0.065,-0.143c-0.059,0.13 -0.119,0.259 -0.179,0.388c-3.338,7.166 -7.776,13.517 -13.314,19.055c-5.537,5.537 -11.889,9.975 -19.055,13.314c-7.166,3.338 -15.064,5.008 -23.696,5.008c-8.469,0 -16.286,-1.67 -23.452,-5.008c-7.166,-3.339 -13.518,-7.777 -19.055,-13.314c-5.345,-5.345 -9.665,-11.447 -12.961,-18.309c-3.137,7.045 -7.326,13.189 -12.568,18.431c-5.456,5.456 -11.93,9.772 -19.421,12.947c-7.492,3.176 -15.472,4.764 -23.941,4.764c-8.469,0 -16.408,-1.588 -23.819,-4.764c-7.41,-3.175 -13.843,-7.491 -19.299,-12.947c-5.456,-5.456 -9.772,-11.889 -12.947,-19.299c-3.176,-7.411 -4.764,-15.35 -4.764,-23.819c0,-8.469 1.588,-16.449 4.764,-23.941c3.175,-7.492 7.491,-13.965 12.947,-19.421c5.456,-5.456 11.889,-9.772 19.299,-12.948c7.411,-3.176 15.35,-4.763 23.819,-4.763c8.469,0 16.449,1.587 23.941,4.763c7.491,3.176 13.965,7.492 19.421,12.948c5.353,5.353 9.608,11.686 12.766,18.998c3.271,-6.695 7.525,-12.661 12.763,-17.899c5.537,-5.537 11.889,-9.975 19.055,-13.314c7.166,-3.339 14.983,-5.008 23.452,-5.008c8.632,0 16.53,1.669 23.696,5.008c7.166,3.339 13.518,7.777 19.055,13.314l-8.55,8.55c-4.235,-4.234 -9.283,-7.736 -15.146,-10.504c-5.863,-2.769 -12.215,-4.153 -19.055,-4.153c-6.678,0 -12.948,1.384 -18.811,4.153c-5.863,2.768 -10.912,6.27 -15.146,10.504c-4.886,4.235 -8.55,9.284 -10.993,15.147c-2.443,5.863 -3.665,12.214 -3.665,19.055c0,6.677 1.222,12.947 3.665,18.81c2.443,5.863 6.107,10.912 10.993,15.146c4.234,4.886 9.283,8.551 15.146,10.994c5.863,2.442 12.133,3.664 18.811,3.664c6.84,0 13.192,-1.222 19.055,-3.664c5.863,-2.443 10.911,-6.108 15.146,-10.994c4.234,-4.234 7.736,-9.283 10.505,-15.146c2.768,-5.863 4.153,-12.133 4.153,-18.81l0,-182.976l12.214,0l0,150.475Zm-271.654,-150.964l0,182.976c0,6.677 1.303,12.948 3.908,18.811c2.606,5.863 6.108,10.993 10.505,15.39c4.397,4.397 9.527,7.899 15.391,10.505c5.863,2.605 12.133,3.908 18.81,3.908l0,12.215c-8.469,0 -16.408,-1.588 -23.819,-4.764c-7.41,-3.175 -13.843,-7.491 -19.299,-12.947c-5.456,-5.456 -9.771,-11.889 -12.947,-19.299c-3.176,-7.411 -4.764,-15.35 -4.764,-23.819l0,-182.976l12.215,0Zm99.183,231.59c6.677,0 12.988,-1.303 18.933,-3.908c5.944,-2.606 11.115,-6.108 15.512,-10.505c4.398,-4.397 7.899,-9.527 10.505,-15.39c2.606,-5.863 3.909,-12.134 3.909,-18.811c0,-6.677 -1.303,-12.988 -3.909,-18.933c-2.606,-5.944 -6.107,-11.115 -10.505,-15.512c-4.397,-4.398 -9.568,-7.899 -15.512,-10.505c-5.945,-2.606 -12.256,-3.909 -18.933,-3.909c-6.677,0 -12.948,1.303 -18.811,3.909c-5.863,2.606 -10.993,6.107 -15.39,10.505c-4.397,4.397 -7.899,9.568 -10.505,15.512c-2.606,5.945 -3.909,12.256 -3.909,18.933c0,6.677 1.303,12.948 3.909,18.811c2.606,5.863 6.108,10.993 10.505,15.39c4.397,4.397 9.527,7.899 15.39,10.505c5.863,2.605 12.134,3.908 18.811,3.908Z\" style=\"fill-rule:nonzero;\"/></svg></h2>"
    "<h3 align=\"center\" style=\"color:gray;margin:10px;\">{{Preset}}</h3>"

    "<table>"
      "<tr><th>Datum/Zeit:</th><td>   {{currentDate}}</td></tr>"
      "<tr><th>Leistung:</th><td>         {{currentValue}} MWh</td></tr>"
    //  "<tr><th>Minimum:</th><td>      {{currentMin}} MWh</td></tr>"
    //  "<tr><th>Maximum:</th><td>      {{currentMax}} MWh</td></tr>"
    //  "<tr><th>Durchschnitt:</th><td> {{currentAvg}} MWh</td></tr>"
      "<tr><th>Farbe:</th><td>        {{currentColor}}</td></tr>"
    "</table>"
    "<p></p><p style=\"padding-top:15px;text-align:center\"><a href=\"javascript:location.reload();\">Neu laden</a></p>"
    "<p></p><p style=\"text-align:center;\">Gehe auf Einstellungen (Zahnrad unten) und den Reiter <b>Datenquellen</b> um die Anzeige zu &auml;ndern.</p>"
    "<p></p><p style=\"padding-top:15px;text-align:center\">" AUTOCONNECT_LINK(COG_24) "</p>"
    "</div></body>"
    "</html>";

  content.replace("{{Timeout}}",      String(presetTimeout * 3000 * 10));
  content.replace("{{Preset}}",       presetTitle);
  content.replace("{{currentDate}}",  currentDate);
  content.replace("{{currentValue}}", String(currentValue, 0));
  content.replace("{{currentMin}}",   String(currentMin, 0));
  content.replace("{{currentMax}}",   String(currentMax, 0));
  content.replace("{{currentAvg}}",   String(currentAvg, 0));
  content.replace("{{currentColor}}", String(currentColorString));

  Server.send(200, "text/html", content);
}

void startPage() {
  // Retrieve the value of AutoConnectElement with arg function of WebServer class.
  // Values are accessible with the element name.
  String  pres = Server.arg("preset");
  Serial.println("Preset: " + pres);

  for (uint8_t n = 0; n < sizeof(PRES) / sizeof(Preset_t); n++) {
    String  paramPresetTitle = String(PRES[n].title);
    if (pres.equalsIgnoreCase(paramPresetTitle)) {
      presetTitle   = PRES[n].title;
      presetServer  = PRES[n].server;
      presetDatakey = PRES[n].datakey;
      presetTimeout = PRES[n].timeout;
      Serial.println("Configured preset: " + presetTitle);

      // Store selected index in preferences
      pref.putUInt("PresetIndex", n);

      break; // Leave this loop
    }
  }

  // The /start page just constitutes timezone,
  // it redirects to the root page without the content response.
  Server.sendHeader("Location", String("http://") + Server.client().localIP().toString() + String("/"));
  Server.send(302, "text/plain", "");
  Server.client().flush();
  Server.client().stop();

}

void setup() {
  delay(1000);
  Serial.begin(115200);
  Serial.println();

  // init pixel
  pixel.setBrightness(LED_BRIGHTNESS);
  pixel.begin();
  pixel.show(); // Initialize all pixels to 'off'

  // get chip ID for unique SSID
  createSSID();
  Serial.printf("Unique SSID: %s\n", ssid);

  // Enable saved past credential by autoReconnect option,
  // even once it is disconnected.
  Config.title = "LoDa";
  Config.autoReconnect = true;
  Config.apid = ssid;
  Config.psk  = "loda1234567890";
  Portal.config(Config);

  // Load aux. page
  Preset.load(AUX_PRESET);
  
  // Retrieve the select element that holds the preset and it
  AutoConnectSelect&  pres = Preset["preset"].as<AutoConnectSelect>();
  for (uint8_t n = 0; n < sizeof(PRES) / sizeof(Preset_t); n++) {
    pres.add(String(PRES[n].title));
  }  
  Portal.join({ Preset });        // Register aux. page

  // Behavior a root path of ESP8266WebServer.
  Server.on("/", rootPage);
  Server.on("/start", startPage);   // Set NTP server trigger handler

  // Establish a connection with an autoReconnect option.
  if (Portal.begin()) {
    Serial.println("WiFi connected: " + WiFi.localIP().toString());
  }


  // init preferences
  pref.begin("iob", false);
  const uint8_t preset_index = pref.getUInt("PresetIndex", 0);
  Serial.printf("Stored preset index: %i\n", preset_index);

  // Get stored preset from preferences
  presetTitle   = PRES[preset_index].title;
  presetServer  = PRES[preset_index].server;
  presetDatakey = PRES[preset_index].datakey;
  presetTimeout = PRES[preset_index].timeout;
  Serial.println("Stored preset: " + presetTitle);
  
}

void loop() {
  static long currentMills;

  if (millis() - currentMills >= (presetTimeout * 1000 * 3)) {
    currentMills = millis();
    
    if (WiFi.status() == WL_CONNECTED) {
      Serial.println("WiFi connected: " + WiFi.localIP().toString());
      
      if(presetTitle.length() > 0) {
        
        Serial.println("Preset Server: " + presetServer);
        // do the magic request
        doRequest( presetServer );
        
      } else {
        
        Serial.println("No Preset");
        pixel.setPixelColor(150, 150, 0, 0);
        pixel.show();
        delay(2000);
        pixel.clear();
      }
    } else {
        
        Serial.println("No WiFi");
        pixel.setPixelColor(0, 200, 0, 0);
        pixel.show();
        delay(2000);
        pixel.clear();
      }
  }
  Portal.handleClient();
}

/**
 * create the unique board ssid from the mac address
 */
void createSSID() {
  uint64_t chipid=ESP.getEfuseMac() ;//The chip ID is essentially its MAC address(length: 6 bytes).
  uint16_t chip_low   = (uint16_t)(chipid>>32);
  snprintf(ssid,15,"LoDa-%04X%08X",chip_low, chipid);
}

/**
 * get the led lightning from url
 */
void doRequest( String url ) {


  if (url) {

    Serial.println("Update data");
  
    HTTPClient rest;
    rest.begin(url);
    int httpCode = rest.GET();

    if (httpCode > 0) {
      String payload = rest.getString();

      const size_t capacity = 2*JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(6) + 120;
      DynamicJsonDocument doc(capacity);
      
      deserializeJson(doc, payload);

      long timestmp = doc["timestmp"]; // 1569245400
      const char* datetime = doc["datetime"]; // "2019-09-23 15:30:00"
      //float value = doc["value"]; // 235.241
      
      JsonObject color = doc["color"];
      int color_r = color["r"]; // 255
      int color_g = color["g"]; // 0
      int color_b = color["b"]; // 43

      currentDate   = String(datetime);
      currentValue  = doc["value"];

      // set current values
      ////JsonObject stats = doc["statistics"];
      currentMin    = doc["min"];
      currentMax    = doc["max"];
      currentAvg    = doc["avg"];
      currentColor  = {color_r, color_g, color_b};
      sprintf(currentColorString, "rgb(%d, %d, %d)", currentColor.r, currentColor.g, currentColor.b);

      Serial.printf("Datetime: %s ", datetime);
      Serial.printf("Color: rgb(%d, %d, %d)\n", color_r, color_g, color_b);
      Serial.printf("Statistics: min = %.2f, max = %.2f, avg = %.2f", currentMin, currentMax, currentAvg);

      colorFade(color_r, color_g, color_b);
    }
  
  }
 }

void colorFade(uint8_t r, uint8_t g, uint8_t b) {
  for(uint16_t i = 0; i < pixel.numPixels(); i++) {
      uint8_t curr_r, curr_g, curr_b;
      uint32_t curr_col = pixel.getPixelColor(i); // get the current colour
      curr_b = curr_col & 0xFF; 
      curr_g = (curr_col >> 8) & 0xFF; 
      curr_r = (curr_col >> 16) & 0xFF;  // separate into RGB components

      while ((curr_r != r) || (curr_g != g) || (curr_b != b)){  // while the curr color is not yet the target color
        if (curr_r < r) curr_r++; else if (curr_r > r) curr_r--;  // increment or decrement the old color values
        if (curr_g < g) curr_g++; else if (curr_g > g) curr_g--;
        if (curr_b < b) curr_b++; else if (curr_b > b) curr_b--;
        pixel.setPixelColor(i, curr_r, curr_g, curr_b);  // set the color
        pixel.show();
      }
      delay(100);
  }
}
